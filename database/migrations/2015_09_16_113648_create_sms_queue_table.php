<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_sms_booking_teacher_list', function (Blueprint $table) {

            $table->increments('id');
            $table->bigInteger('school_sms_booking_id');
            $table->bigInteger('teacher_user_id');
            $table->enum('list_type', ['short', 'waiting'])->default('waiting');
            $table->timestamp('send_time');
            $table->string('phonenumber');
            $table->enum('response', ['', 'accepted', 'declined', 'booked'])->default('');
            $table->timestamp('response_timestamp');
            $table->string('accept_keyword');
            $table->string('decline_keyword');
            $table->enum('status', ['sent','received','rejected','pending'])->default('pending');
            $table->timestamp('sent_at');
            $table->string('sms_private_id');
            $table->string('sms_gateway');
            $table->integer('sms_teacher_order')->default('-1');
            $table->integer('incomplete_profile')->default('0');
            $table->integer('linked_invitation_id')->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('school_sms_booking_teacher_list');
    }
}
