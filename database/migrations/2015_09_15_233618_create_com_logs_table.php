<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('com_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['request', 'response']);
            $table->string('method');
            $table->string('uri');
            $table->string('package_id');
            $table->string('ip');
            $table->text('payload');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('com_logs');
    }
}
