<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SchoolSmsBookingTeacher
 * @package App\Models\Db
 */
class SchoolSmsBookingTeacher extends Model
{
    protected $table = 'school_sms_booking_teacher_list';
}
