<?php

namespace App\Models\Sms;

use App\Models\Sms\Booking;
use App\Models\Db\SchoolSmsBookingTeacher;
use Illuminate\Support\Facades\DB;
/**
 * Class BookingManager
 * @package App\Models\Sms
 */
class BookingManager
{

    private $list = [];
    /**
     * @var BookingManager The reference to *Singleton* instance of this class
     */
    private static $instance;

    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return BookingManager The *Singleton* instance.
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    protected function __construct()
    {
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }

    /**
     * @param \App\Models\Sms\Booking $booking
     * @return bool
     */
    public function process(Booking $booking)
    {

        $referenceTime = strtotime(date("Y-m-d H:i:s"));
        $initialReferenceTime = $referenceTime;

        $teachersToBook = $booking->getTeachersToBook();

        $shortListIndex = 0;

        foreach ($booking->getShortList() as $k => $item) {

            $delay = intval($item['delay']) * 60;

            if ($shortListIndex < $teachersToBook || $booking->getSmsType() == Booking::SMS_TYPE_BLANKET) {
                $delay = 0;
            }

            $listItem = [
                'school_sms_booking_id' => $booking->getCcBookingId(),
                'teacher_id' => $item['teacher_id'],
                'phonenumber' => $item['phonenumber'],
                'delay' => $delay,
                'incomplete' => $item['incomplete'],
                'send_time' => date("Y-m-d H:i:s", $referenceTime + $delay),
                'list_type' => Booking::LIST_TYPE_SHORT,
                'sms_teacher_order' => $shortListIndex
            ];

            $this->populateList($listItem);

            $referenceTime += $delay;

            ++$shortListIndex;
        }

        if($booking->getSmsType() == Booking::SMS_TYPE_BLANKET && count($booking->getWaitingList()) > 0) {
            $referenceTime += $booking->getWaitingListDelay() * 60;
        }

        foreach ($booking->getWaitingList() as $k=> $item) {

            $delay = 0;

            if($booking->getSmsType() == Booking::SMS_TYPE_BLANKET) {
                $delay = 0;
            } else {
                $delay = intval($item['delay'] * 60);
            }

            $listItem = [
                'school_sms_booking_id' => $booking->getCcBookingId(),
                'teacher_id' => $item['teacher_id'],
                'phonenumber' => $item['phonenumber'],
                'delay' => $delay,
                'incomplete' => $item['incomplete'],
                'send_time' => date("Y-m-d H:i:s", $referenceTime + $delay),
                'list_type' => Booking::LIST_TYPE_WAITING,
                'sms_teacher_order' => Booking::TEACHER_ORDER_DEFAULT
            ];

            $this->populateList($listItem);

            $referenceTime += $item['delay'];
        }


        $this->processQueue($initialReferenceTime, $booking);

        return true;
    }

    protected function processQueue($timeFrom, Booking $booking) {

        $startTimestamp = date("Y-m-d H:i:s", $timeFrom);

        /** @var \Illuminate\Database\Query\Builder $queued */
        $queued = DB::table('school_sms_booking_teacher_list')
            ->where('send_time', '>=', $startTimestamp)
            ->where('school_sms_booking_id', $booking->getCcBookingId())
            ->where('status', 'pending')
            ->get();


        var_dump($queued);

        $this->list = [];


    }

    /**
     * @param $item
     * @return bool
     */
    protected function populateList($item) {

        $postFix = str_pad($item['school_sms_booking_id'] % 1000, 3, "0");


        $item['accept_keyword'] = 'Y'.$postFix;
        $item['decline_keyword'] = 'N'.$postFix;
        $listItem['linked_invitation_id'] = 0;

        if ($item['incomplete'] == "true") {
            $item['linked_invitation_id'] = filter_var($item['teacher_id'], FILTER_SANITIZE_NUMBER_INT);
            $item['teacher_user_id'] = -2;
            $listItem['incomplete_profile'] = 1;
        } else {
            $item['teacher_user_id'] = (int) $item['teacher_id'];
        }

        $schoolSmsBookingTeacher = new SchoolSmsBookingTeacher();
        $schoolSmsBookingTeacher->school_sms_booking_id = $item['school_sms_booking_id'];
        $schoolSmsBookingTeacher->teacher_user_id = $item['teacher_id'];
        $schoolSmsBookingTeacher->list_type = $item['list_type'];
        $schoolSmsBookingTeacher->send_time = $item['send_time'];
        $schoolSmsBookingTeacher->phonenumber = $item['phonenumber'];
        $schoolSmsBookingTeacher->accept_keyword = $item['accept_keyword'];
        $schoolSmsBookingTeacher->decline_keyword = $item['decline_keyword'];
        $schoolSmsBookingTeacher->sms_teacher_order = $item['sms_teacher_order'];
        $schoolSmsBookingTeacher->linked_invitation_id = isset($item['linked_invitation_id']) ? $item['linked_invitation_id'] : 0;

        return $schoolSmsBookingTeacher->save();
    }


}