<?php

namespace App\Models\Sms;

/**
 * Class Booking
 * @package App\Models\Sms
 */
class Booking {

    protected $ccBookingId;
    protected $ccSchoolUserId;
    protected $smsType;
    protected $autoRespondIfPositionFilled;
    protected $sendToWaitingListIfRequired;
    protected $notifyDelayNotFilled;
    protected $shortList = [];
    protected $waitingList = [];
    protected $bookingDetails = [];
    protected $waitingListDelay;
    protected $teachersToBook;
    protected $status;
    protected $smsText;
    protected $createdAt;
    protected $updatedAt;

    const SMS_TYPE_BLANKET = 'blanket';
    const SMS_TYPE_PRIORITY = 'priority';

    const LIST_TYPE_SHORT = 'short';
    const LIST_TYPE_WAITING = 'waiting';

    const TEACHER_ORDER_DEFAULT = -1;
    const TEACHER_DELAY_DEFAULT = 0;

    private $mandatory = [
        'sms_id',
        'sms_type',
        'autorespond_if_position_filled',
        'send_to_waiting_if_required',
        'shortlist',
        'waitinglist_delay',
        'notify_delay_not_filled'
    ];

    /**
     * @param array $data
     * @throws \Exception
     */
    public function __construct(array $data)
    {
        $missing = [];

        foreach ($this->mandatory as $field) {
            if (!isset($data[$field])) {
                $missing[] = $field;
            }
        }

        if (count($missing)>0) {
            throw new \Exception('Could not create booking object as one or more mandatory fields are not set [' . implode(",", $missing) . ']');
        }


        $data = $this->preFilter($data);

        $this->ccBookingId = $data['sms_id'];
        $this->ccSchoolUserId = $data['school_user_id'];
        $this->smsType = $data['sms_type'];
        $this->autoRespondIfPositionFilled = $data['autorespond_if_position_filled'];
        $this->sendToWaitingListIfRequired = $data['send_to_waiting_if_required'];
        $this->notifyDelayNotFilled = $data['notify_delay_not_filled'];
        $this->shortList = $data['shortlist'];
        $this->waitingList = $data['waitinglist'];
        $this->waitingListDelay = $data['waitinglist_delay'];
        $this->bookingDetails = $data['booking_details'];
        $this->teachersToBook = $data['teachers_to_book'];
        $this->status = $data['status'];
        $this->smsText = $data['sms_text'];
        $this->createdAt = $data['created_at'];
        $this->updatedAt = date("Y-m-d H:i:s");

    }

    /**
     * @return mixed
     */
    public function getCcBookingId()
    {
        return $this->ccBookingId;
    }

    /**
     * @param mixed $ccBookingId
     */
    public function setCcBookingId($ccBookingId)
    {
        $this->ccBookingId = $ccBookingId;
    }

    /**
     * @return mixed
     */
    public function getCcSchoolUserId()
    {
        return $this->ccSchoolUserId;
    }

    /**
     * @param mixed $ccSchoolUserId
     */
    public function setCcSchoolUserId($ccSchoolUserId)
    {
        $this->ccSchoolUserId = $ccSchoolUserId;
    }

    /**
     * @return mixed
     */
    public function getSmsType()
    {
        return $this->smsType;
    }

    /**
     * @param mixed $smsType
     */
    public function setSmsType($smsType)
    {
        $this->smsType = $smsType;
    }

    /**
     * @return mixed
     */
    public function getAutoRespondIfPositionFilled()
    {
        return $this->autoRespondIfPositionFilled;
    }

    /**
     * @param mixed $autoRespondIfPositionFilled
     */
    public function setAutoRespondIfPositionFilled($autoRespondIfPositionFilled)
    {
        $this->autoRespondIfPositionFilled = $autoRespondIfPositionFilled;
    }

    /**
     * @return mixed
     */
    public function getSendToWaitingListIfRequired()
    {
        return $this->sendToWaitingListIfRequired;
    }

    /**
     * @param mixed $sendToWaitingListIfRequired
     */
    public function setSendToWaitingListIfRequired($sendToWaitingListIfRequired)
    {
        $this->sendToWaitingListIfRequired = $sendToWaitingListIfRequired;
    }

    /**
     * @return mixed
     */
    public function getNotifyDelayNotFilled()
    {
        return $this->notifyDelayNotFilled;
    }

    /**
     * @param mixed $notifyDelayNotFilled
     */
    public function setNotifyDelayNotFilled($notifyDelayNotFilled)
    {
        $this->notifyDelayNotFilled = $notifyDelayNotFilled;
    }

    /**
     * @return array
     */
    public function getShortList()
    {
        return $this->shortList;
    }

    /**
     * @param array $shortList
     */
    public function setShortList($shortList)
    {
        $this->shortList = $shortList;
    }

    /**
     * @return array
     */
    public function getWaitingList()
    {
        return $this->waitingList;
    }

    /**
     * @param array $waitingList
     */
    public function setWaitingList($waitingList)
    {
        $this->waitingList = $waitingList;
    }

    /**
     * @return array
     */
    public function getBookingDetails()
    {
        return $this->bookingDetails;
    }

    /**
     * @param array $bookingDetails
     */
    public function setBookingDetails($bookingDetails)
    {
        $this->bookingDetails = $bookingDetails;
    }

    /**
     * @return mixed
     */
    public function getWaitingListDelay()
    {
        return $this->waitingListDelay;
    }

    /**
     * @param mixed $waitingListDelay
     */
    public function setWaitingListDelay($waitingListDelay)
    {
        $this->waitingListDelay = $waitingListDelay;
    }

    /**
     * @return mixed
     */
    public function getTeachersToBook()
    {
        return $this->teachersToBook;
    }

    /**
     * @param mixed $teachersToBook
     */
    public function setTeachersToBook($teachersToBook)
    {
        $this->teachersToBook = $teachersToBook;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getSmsText()
    {
        return $this->smsText;
    }

    /**
     * @param mixed $smsText
     */
    public function setSmsText($smsText)
    {
        $this->smsText = $smsText;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param array $data
     * @return array
     */
    private function preFilter(array $data)
    {

        if (!isset($data['waitinglist'])) {
            $data['waitinglist'] = array();
        }

        if ($data['autorespond_if_position_filled'] == "true") {
            $data['autorespond_if_position_filled'] = true;
        } else {
            $data['autorespond_if_position_filled'] = false;
        }

        if ($data['send_to_waiting_if_required'] == "true") {
            $data['send_to_waiting_if_required'] = true;
        } else {
            $data['send_to_waiting_if_required'] = false;
        }

        $data['teachers_to_book'] = intval($data['teachers_to_book']);
        $data['waitinglist_delay'] = intval($data['waitinglist_delay']);

        return $data;
    }
}