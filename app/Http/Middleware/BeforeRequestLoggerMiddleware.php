<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Db\ComLog;
use Psy\Util\Json;

class BeforeRequestLoggerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $requestPackageId = uniqid(time(), true);

        /** @var \Illuminate\Http\Response $response */
        $response = $next($request);

        /**
         * @var ComLog $logRequest
         * Will add a log with the request content
         */
        $logRequest = new ComLog();
        $logRequest->type = 'request';
        $logRequest->payload = Json::encode($request->all());
        $logRequest->method = $request->getMethod();
        $logRequest->uri = $request->getUri();
        $logRequest->ip = $request->getClientIp();
        $logRequest->package_id = $requestPackageId;
        $logRequest->setCreatedAt(new \DateTime('NOW', new \DateTimeZone('UTC')));
        $logRequest->save();


        /**
         * @var ComLog $logResponse
         * Will add a log with the response content
         */
        $logResponse = new ComLog();
        $logResponse->type = 'response';
        $logResponse->payload = $response->getContent();
        $logResponse->method = '';
        $logResponse->uri = '';
        $logResponse->ip = $_SERVER['SERVER_ADDR'];
        $logResponse->package_id = $requestPackageId;
        $logRequest->setCreatedAt(new \DateTime('NOW', new \DateTimeZone('UTC')));
        $logResponse->save();

        return $response;
    }
}
