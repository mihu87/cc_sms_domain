<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\JsonResponse;
use App\Models\Sms\Booking;
use App\Models\Sms\BookingManager;

/**
 * Class SmsController
 * @route /rest
 * @package App\Http\Controllers
 */
class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @route GET /sms/
     * @Before("logger.request.before")
     * @return JsonResponse
     */
    public function index()
    {
        $responseArray = [
            'status' => 200,
            'success' => true,
            'data' => [],
            'message' => 'ClassCover SMS Domain API v1.'
        ];

        return new JsonResponse($responseArray, 200);
    }

    /**
     * Show the form for creating a new resource.
     * @route GET /sms/create
     * @return JsonResponse
     */
    public function create()
    {
        $responseArray = [
            'status' => 200,
            'success' => true,
            'data' => [],
            'message' => 'Resource created.'
        ];

        return new JsonResponse($responseArray, 200);
    }

    /**
     * Store a newly created resource in storage.
     * @route POST /sms/
     * @param  Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $responseArray = [
            'statusCode' => 200,
            'status' => 'success',
            'success' => true,
            'data' => [],
            'message' => 'Resource stored.'
        ];

        $booking = new Booking($data);
        $bookingManager = BookingManager::getInstance();

        $process = $bookingManager->process($booking);


        return new JsonResponse($responseArray, 200);
    }

    /**
     * Display the specified resource.
     * @route GET /sms/{id}
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @route GET /sms/{id}/edit
     * @param  int  $id
     * @return JsonResponse
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @route PUT /sms/{id}
     * @param  Request  $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @route DELETE /sms/{id}
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        //
    }
}
